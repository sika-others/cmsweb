from django.contrib import admin

from models import Teaser, TeaserPlugin

admin.site.register(Teaser)
admin.site.register(TeaserPlugin)