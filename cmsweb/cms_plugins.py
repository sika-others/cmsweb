from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.utils.translation import ugettext as _

from models import TeaserPlugin

class TeaserPluginCMSWEB(CMSPluginBase):
    model = TeaserPlugin
    name = _("Teaser CMSWEB")
    render_template = "cmsweb/cms_plugins/teaser.html"

    def render(self, context, instance, placeholder):
        context.update({
            'object':instance.teaser,
            'instance':instance,
            'placeholder':placeholder,
        })  
        return context

plugin_pool.register_plugin(TeaserPluginCMSWEB)
