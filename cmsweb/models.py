from django.db import models
from cms.models import CMSPlugin, Page

GALLERY_TYPES = (("gallery", "Gallery"), ("slider", "Slider"), )

class Teaser(models.Model):
    title = models.CharField(max_length=128)
    description = models.TextField()
    img = models.ImageField(upload_to="cmsweb/tease/img", null=True, blank=True)
    page_link = models.ForeignKey(Page, blank=True, null=True,
                                  related_name="cmswe_teaser_page_link",
                                  limit_choices_to={'publisher_is_draft': True}, )   
    url = models.CharField("link", max_length=255, blank=True, null=True, )

    def get_link(self):
        if self.page_link:
            return self.page_link.get_absolute_url()
        return self.url

    def __unicode__(self):
        return u"%s ~%i" % (self.title, self.pk)

class TeaserPlugin(CMSPlugin):
    teaser = models.ForeignKey(Teaser)

    def __unicode__(self):
        return u"%s ~%i" % (self.teaser.title, self.teaser.pk)